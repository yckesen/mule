FROM ubuntu:16.04

MAINTAINER heb "info@heb.ch"

RUN apt-get update && apt-get install -y openjdk-8-jdk 

COPY ./mule-enterprise-standalone-4.2.1-hf1 /opt/mule/mule-enterprise-standalone-4.2.1-hf1

CMD ["/bin/bash"]
